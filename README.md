# mithril.js_examples

Simple mithril.js examples

Mithril is the most amazing JavaScript framework, with just one problem:
The people that use it are too intelligent. They don't understand how
steep the learning curve is for those that don't understand the problems
with node.js.

Those that are talented enough to use mithril.js seem not to flood
stackoverflow with questions; which leads to a lack of examples.

That is why I wanted to promote my favourite framework by posting examples
that can be cut-n-pasted. (All code examples are released under the same
License as mithril.js itself: The MIT License; all documentation released
 under the Creative Commons Attribution 4.0 International License.)

If you _have_ to include attribution:
// rdfa:src="gitlab.com/iso-i/mithril.js_examples"
